// Copyright (c) Copyright (c) Hercules Dev Team, licensed under GNU GPL.
// Copyright (c) 2014 - 2015 Evol developers

#ifndef EVOL_MAP_SCRIPT_BUILDINS
#define EVOL_MAP_SCRIPT_BUILDINS

BUILDIN(l);
BUILDIN(lg);
BUILDIN(setCamNpc);
BUILDIN(setCam);
BUILDIN(moveCam);
BUILDIN(restoreCam);
BUILDIN(npcTalk3);
BUILDIN(closeDialog);
BUILDIN(shop);
BUILDIN(getItemLink);
BUILDIN(requestLang);
BUILDIN(requestItem);
BUILDIN(requestItems);
BUILDIN(requestItemIndex);
BUILDIN(requestItemsIndex);
BUILDIN(requestCraft);
BUILDIN(getq);
BUILDIN(setq);
BUILDIN(setNpcDir);
BUILDIN(rif);
BUILDIN(miscEffect);
BUILDIN(setMapMask);
BUILDIN(getMapMask);
BUILDIN(addMapMask);
BUILDIN(removeMapMask);
BUILDIN(setNpcSex);
BUILDIN(showAvatar);
BUILDIN(setAvatarDir);
BUILDIN(setAvatarAction);
BUILDIN(clear);
BUILDIN(changeMusic);
BUILDIN(setNpcDialogTitle);
BUILDIN(getMapName);
BUILDIN(unequipById);
BUILDIN(isPcDead);
BUILDIN(areaTimer);
BUILDIN(getAreaDropItem);
BUILDIN(setMount);
BUILDIN(clientCommand);
BUILDIN(isUnitWalking);
BUILDIN(failedRefIndex);
BUILDIN(downRefIndex);
BUILDIN(successRefIndex);
BUILDIN(successRemoveCardsIndex);
BUILDIN(failedRemoveCardsIndex);
BUILDIN(isStr);
BUILDIN(npcSit);
BUILDIN(npcStand);
BUILDIN(npcWalkTo);
BUILDIN(setBgTeam);
BUILDIN(chatJoin);
BUILDIN(checkNpcCell);
BUILDIN(setCells);
BUILDIN(delCells);
BUILDIN(setSkin);
BUILDIN(initCraft);
BUILDIN(dumpCraft);
BUILDIN(deleteCraft);
BUILDIN(getCraftSlotId);
BUILDIN(getCraftSlotAmount);
BUILDIN(validateCraft);
BUILDIN(getInvIndexLink);
BUILDIN(emotion);
BUILDIN(findCraftEntry);
BUILDIN(useCraft);
BUILDIN(getCraftCode);
BUILDIN(setLook);
BUILDIN(htNew);
BUILDIN(htGet);
BUILDIN(htPut);
BUILDIN(htClear);
BUILDIN(htDelete);
BUILDIN(htSize);
BUILDIN(htIterator);
BUILDIN(htiNextKey);
BUILDIN(htiCheck);
BUILDIN(htiDelete);

#endif  // EVOL_MAP_SCRIPT_BUILDINS
